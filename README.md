# CI test for C# solutions

- Le fichier `.gitlab-ci.yml` définit la pipeline du runner gitlab.
- Le fichier `setup.js`, est une rustine pour copier et changer la dll avant de lancer le build. 

> Il existe d'autre moyen plus standard de changer la dll avant le build. Ca a été fait 

## Installation 

- Copier le dossier `ci` dans votre projet
- Copier le fichier `.gitlab-ci.yml` dans votre projet
- Dans le fichier `.gitlab-ci.yml` changer le nom du fichier .sln pour correspondre au votre :

![Acces public gitlab](./doc/sln-ex.png)

- Mettre le projet en public :

![Acces public gitlab](./doc/access-public.png)


A partir de là, chaque commit sur la branche master sera automatiquement construit en un executable que vous pourrez ensuite recuperer dans les artifacts du job correspondant comme suit :

![Telecharge l'executable](./doc/download-artifact.png)
