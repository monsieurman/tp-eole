﻿using System;
using System.Threading;
using MccDaq;

namespace TestMccBoard
{
	class TestMccBoard
	{
		static void Main(string[] args)
		{
			MccBoard acqBoard = new MccBoard(0);

			if (acqBoard != null)
			{
				#region hello les mesures
				ushort mes = 0;
				acqBoard.AIn(6, Range.Bip10Volts, out mes);
				Console.WriteLine("L = {0}", mes);
				Console.Write("Fin Hello le mesures.\nUne touche pour continuer.");
				Console.ReadKey();
				#endregion

				#region Partie 2
				int i = 0;
				float volt = 0.0f;

				while (true)
				{
					Console.Write("[{0}] --> ", i++);
					for (int voie = 6; voie < 8; voie++)
					{
						acqBoard.AIn(voie, Range.Bip10Volts, out mes);
						//acqBoard.ToEngUnits(Range.Bip10Volts, mes, out volt);
						acqBoard.VIn(voie, Range.Bip10Volts, out volt, 0);
						Console.WriteLine("Voie {0} = {1} -> {2} V, ", voie, mes, volt);
					}
					Console.WriteLine();
					Thread.Sleep(1000);
				}
			}
			#endregion
		}
	}
}
